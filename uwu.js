/**
	BSD 3-Clause License

	Copyright (c) 2022, Opalium <opalium@opal.bot>
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	this list of conditions and the following disclaimer in the documentation
	and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	contributors may be used to endorse or promote products derived from
	this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Auto-UwUifying bot for Discord. Because somebody had to do this eventually. Might as well be me.
 *
 * I am not sorry.
 * I will never be.
 */

const Discord = require('discord.js');
const { Webhook, Guild, GuildChannel, TextChannel, MessageMentions } = require('discord.js');
const { Uwuifier } = require("uwuifier");
const config = require("./config.js");

const client = new Discord.Client({ intents: [ Discord.Intents.FLAGS.GUILDS, Discord.Intents.FLAGS.GUILD_WEBHOOKS, Discord.Intents.FLAGS.GUILD_MESSAGES ] });
const uwu = new Uwuifier();

/**
 * To avoid messages from different people being grouped together, we're going to use the ol' SBridge
 * trick and utilize two different hooks. Crude, but does the job.
 */
const minHooks = 2;

/** @type Map<string, Webhook[]> */
const webhooks = new Map();
/** @type Map<string, number> */
const currentHooks = new Map();

// Credits: https://stackoverflow.com/questions/7313395/case-insensitive-replace-all
String.prototype.replaceAll = function(strReplace, strWith) {
	var esc = strReplace.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
	var reg = new RegExp(`\\b${esc}\\b`, "ig");
	return this.replace(reg, strWith);
};

client.on("ready", async () => {
	console.log(`Logged in to Discord as ${client.user.tag}!`);

	if (client.guilds.cache.size <= 0) {
		console.error("We aren't part of any guild. Nothing to do.");
		process.exit(1);
	}

	for (let [guildId, channels] of config.targets) {
		let guild = client.guilds.resolve(guildId)
		if (!guild) {
			console.log(`Guild ID ${guildId} was not found, or we are not part of it. Skipping.`);
			continue;
		}

		console.log(`Initializing guild: ${guildId}`);

		for (let channelId of channels) {
			/** @type GuildChannel */
			let channel = guild.channels.resolve(channelId);
			if (!channel || !channel.isText()) {
				console.log(`\tChannel ${channelId} was not found or is not a text channel. Ignoring.`);
				continue;
			}
	
			let hooks = await channel.fetchWebhooks();
			if (!hooks) continue;
	
			let owoHooks = [];
			for (let [hookId, hook] of hooks) {
				if (hook.name.indexOf("owo") > -1) { // Code I never thought I'll write #37
					owoHooks.push(hook);
				}
			}
			
			for (let i = owoHooks.length; i < minHooks; i++) {
				let newHook = await channel.createWebhook(`owo${i}`, { reason: "Let the uwuifying commence." });
				if (!newHook) {
					console.log(`\tFailed to create a new hook in channel ${channel.name} (${channel.id}); this is most likely a permissions issue.`);
					break;
				}

				owoHooks.push(newHook);
			}

			if (owoHooks.length >= minHooks) {
				webhooks.set(channel.id, owoHooks);
				currentHooks.set(channel.id, 0);
				console.log(`\tSuccessfully cached ${owoHooks.length} hOWOks in channel ${channel.name} (${channel.id}).`); // Hahahahahahaha end me
			} else {
				console.log(`\tFailed to register channel ${channel.name} (${channel.id}). Please check the webhooks there, and try creating them manually.`);
			}
		}
	}
});

client.on("messageCreate", async msg => {
	if (msg.webhookID || msg.author.bot) return; // Don't touch anything that is not human. Ew.
	if (!msg.content) return; // Ignore empty messages (embeds, etc.)

	let channel = msg.channel;
	if (!channel) return;

	let hooks = webhooks.get(channel.id);
	let currentHook = currentHooks.get(channel.id) || 0;
	if (!hooks || currentHook === undefined) return;

	let hook = hooks[currentHook];
	currentHooks.set(channel.id, (currentHook + 1) % hooks.length); // Alternate between entries using modulo

	// Begin 'improving' the message
	let processed = msg.content;
	//console.log("Processing message: " + processed)

	// Replace any of the hard-coded strings first, before they become unrecOwOgnizable
	for (let entry of config.replacements) {
		let terms = entry.terms;
		let replace = entry.replace;

		for (let term of terms) {
			processed = processed.replaceAll(term, replace);
		}
	}

	// Scrub off any @everyone, @here, or role mentions because I'm not fucking handling this shit
	processed = processed.replace(`<@${msg.guild.id}>`, "@evwwywne-chan");
	processed = processed.replace("@everyone", "@evwwywne-chan");
	processed = processed.replace("@here", "@heww-chan");
	processed = processed.replace(MessageMentions.ROLES_PATTERN, "@role-chan");
	// Now process and member mentions and add some weeb 'magic' to them
	processed = processed.replace(MessageMentions.USERS_PATTERN, "<@$1>-chan")

	// And now... let all hell break loose.
	processed = uwu.uwuifySentence(processed);
	
	//console.log("Result: " + processed);

	let name = msg.member.displayName;
	if (config.uwuifyNames) {
		name = uwu.uwuifyWords(name);
	}

	const avatarURL = msg.member ? msg.member.displayAvatarURL() : msg.author.displayAvatarURL();

	// Delete the original message and send our... 'enhanced' version instead
	// FIXME: This is prooooobably not a very good idea in terms of rate limits, buuuuut... it's just one day, ya' know?
	await msg.delete();
	hook.send({
		content: processed,
		username: name,
		avatarURL: avatarURL,
		split: true
	});
});

// Some process control stuff
function die() {
	console.log("Bwye bwye OwO! :3");
	client.destroy();
	process.exit(0);
}
process.on('exit', die); // Normal(?) exiting
process.on("SIGINT", die); // Keyboard interruption (Ctrl+C)

// ART
console.info(`
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣿⣿⡆⠀⠀⢸⣷⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⡇⠀⠀⣾⣿⡆⠀
⠀⠀⠀⠀⠀⠀⣿⣿⡇⠀⠀⢸⣿⢰⣿⡆⠀⣾⣿⡆⠀⣾⣷ ⣿⣿⡇⠀⠀⣿⣿⡇⠀
⠀⠀⠀⠀⠀⠀⣿⣿⡇⠀⠀⢸⣿⠘⣿⣿⣤⣿⣿⣿⣤⣿⡇⢻⣿⡇⠀⠀⣿⣿⡇⠀
⠀⠀⠀⠀⠀⠀⣿⣿⡇⠀⠀⢸⡿⠀⢹⣿⣿⣿⣿⣿⣿⣿⠁⢸⣿⣇⠀⢀⣿⣿⠇⠀
⠀⠀⠀⠀⠀⠀⠙⢿⣷⣶⣶⡿⠁⠀⠈⣿⣿⠟⠀⣿⣿⠇⠀⠈⠻⣿⣶⣾⡿⠋⠀⠀

⠀⠀⠀UwUcord - Auto-UwUify Discord Bot
⠀⠀⠀⠀"I'm not sorry. I'll never be."
⠀⠀⠀⠀⠀⠀⠀⠀~Opalium, April 2021

------------------------------------------
`);

// Here goes nothing...
client.login(config.token);
