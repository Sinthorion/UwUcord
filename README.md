⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀  
⠀⠀⣿⣿⡆⠀⠀⢸⣷⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⡇⠀⠀⣾⣿⡆⠀  
⠀⠀⣿⣿⡇⠀⠀⢸⣿⢰⣿⡆⠀⣾⣿⡆⠀⣾⣷ ⣿⣿⡇⠀⠀⣿⣿⡇⠀  
⠀⠀⣿⣿⡇⠀⠀⢸⣿⠘⣿⣿⣤⣿⣿⣿⣤⣿⡇⢻⣿⡇⠀⠀⣿⣿⡇⠀  
⠀⠀⣿⣿⡇⠀⠀⢸⡿⠀⢹⣿⣿⣿⣿⣿⣿⣿⠁⢸⣿⣇⠀⢀⣿⣿⠇⠀  
⠀⠀⠙⢿⣷⣶⣶⡿⠁⠀⠈⣿⣿⠟⠀⣿⣿⠇⠀⠈⠻⣿⣶⣾⡿⠋⠀⠀  
  
⠀⠀⠀UwUcord - Auto-UwUify Discord Bot  
⠀⠀⠀⠀⠀⠀"I'm not sorry. I'll never be."  

**A Discord bot that automatically UwUifies chat messages.**
Supports multiple guilds and channels, pre-defiend replacements, and even automatically set itself up.  
Powered by [discord.js](https://discord.js.org/).

Originally made as my 2021 April Fools prank for my Discord server, but now you too can bring horror to your servers.  
_Somebody had to do this eventually. Might as well be me._


## Installation
UwUcord requires Node.js v16.6 or above. There are a bunch of optional packages that may help performance (the same packages recommended by Discord.js), but they require native build tools.  

1. Clone the repo.
2. Run `npm install` to install dependencies.
3. Edit config.js and fill in your bot token, target guilds and channels. Tweak any other settings you'd like. There are comments in the file to help you understand what's going on.
4. Run UwUcord: `node uwu.js`

_**A note about npm & yarn:** one of the dependencies decided to enforce npm installation ('please-use-npm') for some god-forsaken reason and I can't figure out how to disable it. Unless you find out how, make sure to install with npm._
