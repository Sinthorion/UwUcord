/**
	BSD 3-Clause License

	Copyright (c) 2022, Opalium <opalium@opal.bot>
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

	1. Redistributions of source code must retain the above copyright notice, this
	list of conditions and the following disclaimer.

	2. Redistributions in binary form must reproduce the above copyright notice,
	this list of conditions and the following disclaimer in the documentation
	and/or other materials provided with the distribution.

	3. Neither the name of the copyright holder nor the names of its
	contributors may be used to endorse or promote products derived from
	this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Your Discord bot token.
 * 
 * This bot should be part of the guilds you define below, and needs the following permissions in the target channels:
 *  - Reading messages
 *  - Deleting messages
 *  - Creating webhooks
 */
const token = "";

/**
 * Target guilds and channels to uwuify.
 * The key should be the guild ID, and the value should be an array of channel IDs.
 * 
 * Two webhOwOks will be created in each of these channels (unless they already exist), with the names "owo1" and "owo2".
 */
const targets = new Map();
targets.set("531782547475267584", [
	"531782547902955530", "531783344677978122", "583371472949477377", "807603702080602151",
]);

/**
 * Should the names on the 'improved' messages be uwuified as well?
 */
const uwuifyNames = true;

/**
 * Pre-defined replacements.
 * Each of the strings defined in 'terms' will be automatically replaced by 'replace' *before* any further uwuifying.
 * 
 * Please note that this is a whole-word replacement: If the term is "opal",
 * the word "opal" will be replaced, but not "opalium" or "opalicious".
 */
const replacements = [
	{
		terms: ["<@!312396972596396034>", "<@312396972596396034>", "opawbot", "opalbot", "ob"],
		replace: "OwOpawBot-Senpai >:3"
	},
	{
		terms: ["<@!129332207167864833>", "<@129332207167864833>", "opalium", "opal", "opul", "owopal"],
		replace: "OwOpawium-Senpai c:"
	},
	{
		terms: ["crescent", "cttt", "crs", "crescentttt"],
		replace: "Cwweswent UwU"
	},
]

// Don't touch these
module.exports.token = token;
module.exports.uwuifyNames = uwuifyNames;
module.exports.targets = targets;
module.exports.replacements = replacements;
